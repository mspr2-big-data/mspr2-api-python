from flask import Flask, request, jsonify, make_response
import pandas as pd
from sqlalchemy import create_engine
import statsmodels.formula.api as smf
from os.path import exists
import pickle
import tensorflow as tf
import logging
import psycopg2
logging.basicConfig(filename='example.log', encoding='utf-8', level=logging.DEBUG)

COULEURS = [1, 2, 3, 4, 5, 6]

# Création de l'API Flask
app = Flask("app")
@app.route('/predict', methods=['GET'])
def predict():
    # Récupération des paramètres
    couleur = int(request.args.get('couleur') or 0)
    codedep = int(request.args.get('codedep') or 0)
    annee = int(request.args.get('annee') or 0)
    populationtotale = int(request.args.get('populationtotale') or 0)
    inscritslistes = int(request.args.get('inscritslistes') or 0)
    soldemigratoire = int(request.args.get('soldemigratoire') or 0)
    soldenaturel = int(request.args.get('soldenaturel') or 0)
    nbrchomeur = int(request.args.get('nbrchomeur') or 0)
    admisbacgen = int(request.args.get('admisbacgen') or 0)
    prcadmisbacgen = float(request.args.get('prcadmisbacgen') or 0)
    admisbactech = int(request.args.get('admisbactech') or 0)
    prcadmisbactech = float(request.args.get('prcadmisbactech') or 0)
    admisbacpro = int(request.args.get('admisbacpro') or 0)
    prcadmisbacpro = float(request.args.get('prcadmisbacpro') or 0)
    admisbac = int(request.args.get('admisbac') or 0)
    prcadmisbac = float(request.args.get('prcadmisbac') or 0)
    rdb = float(request.args.get('rdb') or 0)
    tauxepargne = float(request.args.get('tauxepargne') or 0)
    tauxepagnefinancier = float(request.args.get('tauxepagnefinancier') or 0)
    rdbparuc = float(request.args.get('rdbparuc') or 0)
    paparrdb = float(request.args.get('paparrdb') or 0)
    pardbparuc = float(request.args.get('pardbparuc') or 0)
    
    # Vérification des paramètres
    if (codedep == 0 or couleur == 0 or annee == 0 or populationtotale == 0 or inscritslistes == 0 or soldemigratoire == 0 or soldenaturel == 0 or nbrchomeur == 0 or admisbacgen == 0 or prcadmisbacgen == 0 or admisbactech == 0 or prcadmisbactech == 0 or admisbacpro == 0 or prcadmisbacpro == 0 or admisbac == 0 or prcadmisbac == 0 or rdb == 0 or tauxepargne == 0 or tauxepagnefinancier == 0 or rdbparuc == 0 or paparrdb == 0 or pardbparuc == 0):
        return jsonify({
            'error': "Veuillez renseigner les bons paramètres",
            'params': {
                'couleur': couleur,
                'codedep': codedep,
                'annee': annee,
                'populationtotale': populationtotale,
                'inscritslistes': inscritslistes,
                'soldemigratoire': soldemigratoire,
                'soldenaturel': soldenaturel,
                'nbrchomeur': nbrchomeur,
                'admisbacgen': admisbacgen,
                'prcadmisbacgen': prcadmisbacgen,
                'admisbactech': admisbactech,
                'prcadmisbactech': prcadmisbactech,
                'admisbacpro': admisbacpro,
                'prcadmisbacpro': prcadmisbacpro,
                'admisbac': admisbac,
                'prcadmisbac': prcadmisbac,
                'rdb': rdb,
                'tauxepargne': tauxepargne,
                'tauxepagnefinancier': tauxepagnefinancier,
                'rdbparuc': rdbparuc,
                'paparrdb': paparrdb,
                'pardbparuc': pardbparuc
            }
        })
    # Récupération du modèle
    if(exists('models/ancova_parti_' + str(couleur) + '.pickle') == False):
        return 'Le modèle pour la couleur ' + str(couleur) + ' n\'existe pas'
    with open('models/ancova_parti_' + str(couleur) + '.pickle', 'rb') as file:
        model = pickle.load(file)

    # Prédiction
    pred = model.predict(pd.DataFrame({
        'codedep': [codedep],
        'annee': [annee],
        'populationtotale': [populationtotale],
        'inscritslistes': [inscritslistes],
        'soldemigratoire': [soldemigratoire],
        'soldenaturel': [soldenaturel],
        'nbrchomeur': [nbrchomeur],
        'admisbacgen': [admisbacgen],
        'prcadmisbacgen': [prcadmisbacgen],
        'admisbactech': [admisbactech],
        'prcadmisbactech': [prcadmisbactech],
        'admisbacpro': [admisbacpro],
        'prcadmisbacpro': [prcadmisbacpro],
        'admisbac': [admisbac],
        'prcadmisbac': [prcadmisbac],
        'rdb': [rdb],
        'tauxepargne': [tauxepargne],
        'tauxepagnefinancier': [tauxepagnefinancier],
        'rdbparuc': [rdbparuc],
        'paparrdb': [paparrdb],
        'pardbparuc': [pardbparuc]
    }))

    # Retour de la prédiction
    return jsonify({
        'prediction': round(pred[0]), 
        'couleur': couleur
    })

# Prédit un dataframe en fonction du model choisi
def batch_predict_data(data, name, couleur = 'total'):
    if (len(data) == 0):
        raise Exception('No Data available in DB')
    if (name == 'ancova'):
        logging.debug("Model ANCOVA")
        with open('models/ancova_' + couleur + '.pickle', 'rb') as file:
            model = pickle.load(file)
    elif (name == 'sklearn'):
        logging.debug("Model SKLEARN")
        with open('models/sklearn_' + couleur + '.pickle', 'rb') as file:
            model = pickle.load(file)
    elif (name == 'deep'):
        logging.debug("Model DEEP")
        model = tf.keras.models.load_model('models/dp_' + couleur + '.h5')
    else:
        raise Exception('Model not found')
    logging.debug("Model Loaded")
    data["nbrvoix"] = model.predict(data.drop(labels=["nbrvoix", "model", "type", "id", "nomdep"], axis=1))
    return data

def batch_predict_global(name):
    logging.debug("Connexion DB")
    engine = create_engine("postgresql+psycopg2://homkizz:!PQ*qF59c3?z6d/T@144.76.83.87:5432/MainDB")
    with engine.connect() as conn, conn.begin():
        data = pd.read_sql_query('SELECT * FROM public.predictions;', conn)
    data = batch_predict_data(data, name)
    conn = psycopg2.connect(
        host="144.76.83.87",
        port=5432,
        database="MainDB",
        user="homkizz",
        password="!PQ*qF59c3?z6d/T"
    )
    sql_update_query = "UPDATE public.predictions SET nbrvoix=%s, model=%s, type=%s WHERE id=%s;"
    with conn.cursor() as cur:
        for index, row in data.iterrows():
            cur.execute(sql_update_query, (round(row['nbrvoix']), name, 'global', row['id']))
    conn.commit()
    conn.close()

def batch_predict_parti(name):
    for couleur in COULEURS:
        logging.debug("Connexion DB")
        engine = create_engine("postgresql+psycopg2://homkizz:!PQ*qF59c3?z6d/T@144.76.83.87:5432/MainDB")
        with engine.connect() as conn, conn.begin():
            data = pd.read_sql_query('SELECT * FROM public.predictions WHERE id_couleur = ' + str(couleur) +  ';', conn)
        data = batch_predict_data(data, name, 'parti_' + str(couleur))
        conn = psycopg2.connect(
            host="144.76.83.87",
            port=5432,
            database="MainDB",
            user="homkizz",
            password="!PQ*qF59c3?z6d/T"
        )
        sql_update_query = "UPDATE public.predictions SET nbrvoix=%s, model=%s, type=%s WHERE id=%s;"
        with conn.cursor() as cur:
            for index, row in data.iterrows():
                cur.execute(sql_update_query, (round(row['nbrvoix']), name, str(couleur), row['id']))
        conn.commit()
        conn.close()

@app.route('/batch-predict', methods=['GET'])
def batch_predict():
    try:
        name = request.args.get('model') or 'ancova'
        type = request.args.get('type') or 'global'
        if (type == 'global'):
            batch_predict_global(name)
        else:
            batch_predict_parti(name)
        return make_response("OK", 200)
    except Exception as e:
        logging.debug(e)
        return make_response("NOT OK", 500)
    

if __name__ == '__main__':
    app.run()
